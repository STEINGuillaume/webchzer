<?php

namespace AppBundle\ZipStream;

use AppBundle\Leechzer\ZipFile;
use ZipStream\ZipStream as BaseZipStream;

class ZipStream extends BaseZipStream
{
    const OPTION_CONTENT_LENGTH = 'content_length';

    public function __construct($name = null, array $opt = array())
    {
        $opt = array_merge([
            'content_type' => 'application/octet-stream',
        ], $opt);
        parent::__construct($name, $opt);
    }

    /**
     * @param ZipFile[] $files
     */
    public function setContent($files)
    {
        $this->setSize($files);
        //loop keys - useful for multiple files
        foreach ($files as $track) {
            $this->addZipFile($track);
        }
    }

    /**
     * @param ZipFile[] $files
     */
    public function setSize($files)
    {
        $zip_size = 0;
        foreach ($files as $file) {
            // Local file header (no extra fields)
            $zip_size += 0x1E;
            $zip_size += strlen($file->getPath());
            // File data
            $zip_size += $file->getSize();
            // Data descriptor signature
            $zip_size += 0x04;
            // Data descriptor
            $zip_size += 0x0C;
            // Central file header (no extra fields, no comment)
            $zip_size += 0x2E;
            $zip_size += strlen($file->getPath());
        }
        // End of central directory record
        $zip_size += 0x16;

        $this->opt[self::OPTION_CONTENT_LENGTH] = $zip_size;
    }

    /**
     * @param ZipFile $file
     */
    private function addZipFile($file)
    {
        $fp = fopen($file->getUrl(), 'rb');
        $this->addFileFromStream($file->getPath(), $fp);
        fclose($fp);
    }

    /**
     *  Send HTTP headers for this stream.
     *
     * @return void
     */
    protected function sendHttpHeaders()
    {
        // grab options
        $opt = $this->opt;

        // grab content type from options
        $content_type = 'application/x-zip';
        if (isset($opt[self::OPTION_CONTENT_TYPE])) {
            $content_type = $this->opt[self::OPTION_CONTENT_TYPE];
        }

        // grab content disposition
        $disposition = 'attachment';
        if (isset($opt[self::OPTION_CONTENT_DISPOSITION])) {
            $disposition = $opt[self::OPTION_CONTENT_DISPOSITION];
        }

        if ($this->output_name) {
            $disposition .= "; filename=\"{$this->output_name}\"";
        }

        $headers = array(
            'Content-Type' => $content_type,
            'Content-Disposition' => $disposition,
            'Pragma' => 'public',
            'Cache-Control' => 'public, must-revalidate',
            'Content-Transfer-Encoding' => 'binary'
        );

        if (isset($opt[self::OPTION_CONTENT_LENGTH])) {
            $headers['Content-Length'] = $opt[self::OPTION_CONTENT_LENGTH];
        }

        $call = $this->opt[self::OPTION_HTTP_HEADER_CALLBACK];
        foreach ($headers as $key => $val) {
            $call("$key: $val");
        }
    }
}
