<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    const QUALITY_MP3_128 = 'MP3_128';
    const QUALITY_MP3_192 = 'MP3_192';
    const QUALITY_MP3_256 = 'MP3_256';
    const QUALITY_MP3_320 = 'MP3_320';
    const QUALITY_FLAC    = 'FLAC';
    const DEFAULT_QUALITY = self::QUALITY_MP3_320;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(length=50)
     * @var string
     */
    protected $quality = self::DEFAULT_QUALITY;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $zip_tracks = false;

    /**
     * @ORM\Column(type="boolean")
     * @var bool
     */
    protected $zip_add_cover = false;

    /**
     * @return bool
     */
    public function isWhiteListed()
    {
        return $this->hasRole('ROLE_WHITE_LISTED') or $this->hasRole('ROLE_ADMIN');
    }

    /**
     * @return array
     */
    public static function getQualities()
    {
        return [
            'user.quality.mp3.128' => self::QUALITY_MP3_128,
            'user.quality.mp3.192' => self::QUALITY_MP3_192,
            'user.quality.mp3.256' => self::QUALITY_MP3_256,
            'user.quality.mp3.320' => self::QUALITY_MP3_320,
            'user.quality.flac'    => self::QUALITY_FLAC,
        ];
    }

    /**
     * @param User $user
     * @return string
     */
    public static function getUserQuality(User $user = null)
    {
        return ($user === null ? self::DEFAULT_QUALITY : $user->getQuality());
    }

    /**
     * @param User $user
     * @return bool
     */
    public static function getUserZipTracks(User $user = null)
    {
        return ($user === null ? false : $user->getZipTracks());
    }

    /**
     * @param User $user
     * @return bool
     */
    public static function getUserCover(User $user = null)
    {
        return ($user === null ? false : $user->getZipAddCover());
    }

    /**
     * @return string
     */
    public function getQuality()
    {
        return $this->quality;
    }

    /**
     * @return string
     */
    public function getQualityTransKey()
    {
        return array_keys(self::getQualities(), $this->getQuality())[0];
    }

    /**
     * @param string $quality
     */
    public function setQuality($quality)
    {
        $this->quality = $quality;
    }


    /**
     * @return bool
     */
    public function getZipTracks()
    {
        return $this->zip_tracks;
    }

    /**
     * @param bool $zip_tracks
     */
    public function setZipTracks($zip_tracks)
    {
        $this->zip_tracks = $zip_tracks;
    }

    /**
     * @return bool
     */
    public function getZipAddCover()
    {
        return $this->zip_add_cover;
    }

    /**
     * @param bool $zip_add_cover
     */
    public function setZipAddCover($zip_add_cover)
    {
        $this->zip_add_cover = $zip_add_cover;
    }
}
