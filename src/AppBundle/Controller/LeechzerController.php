<?php

namespace AppBundle\Controller;

use AppBundle\Leechzer\Cover;
use AppBundle\Leechzer\Track as LeechzerTrack;
use AppBundle\Leechzer\Album as LeechzerAlbum;
use AppBundle\Leechzer\ProxyResponse as LeechzerProxyResponse;
use AppBundle\ZipStream\ZipStream;
use AppBundle\Entity\User;
use DeezerAPI\Models\Track as DeezerTrack;
use DeezerAPI\Models\Album as DeezerAlbum;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\Request;

class LeechzerController extends Controller
{
    /**
     * @Route("/stream/track/{id}", name="streamTrack", requirements={"id": "\d+"})
     * @param Request $request
     * @param $id
     * @return LeechzerProxyResponse
     */
    public function streamTrackAction(Request $request, $id)
    {
        $dztrack = new DeezerTrack($id);
        $track = new LeechzerTrack(
            $this->getParameter('leechzer_entry_point'),
            User::getUserQuality($this->getUser()),
            $dztrack->id,
            $dztrack->title,
            $dztrack->album->title,
            $dztrack->artist->name
        );

        $forbidden=['/','<','>',':','"','\\','|','?','*'];
        $replacedBy='-';

        $response = new LeechzerProxyResponse($track->getUrl(), $request->headers);

        $response->headers->set(
            'Content-Disposition',
            $response->headers->makeDisposition(
                'attachment',
                str_replace($forbidden, $replacedBy, $track->getName()),
                $id . $track->getExtension() // ascii filename fallback for unsupported browsers
            )
        );
        return $response;
    }

    /**
     * @Route("/download/track/{id}", name="downloadTrack", requirements={"id": "\d+"})
     * @param $id
     * @return Response|StreamedResponse
     */
    public function downloadTrackAction($id)
    {
        if (!User::getUserZipTracks($this->getUser())) {
            return $this->forward('AppBundle:Leechzer:streamTrack', [
                'id' => $id
            ]);
        }

        $dztrack = new DeezerTrack($id);
        $response = new StreamedResponse(function () use ($dztrack) {
            $zip = new ZipStream($dztrack->title.'.zip');
            $zip->setContent(array_merge(
                [new LeechzerTrack(
                    $this->getParameter('leechzer_entry_point'),
                    User::getUserQuality($this->getUser()),
                    $dztrack->id,
                    $dztrack->title,
                    $dztrack->album->title,
                    $dztrack->artist->name
                )],
                (User::getUserCover($this->getUser())?[new Cover($dztrack->album, $dztrack->artist)]:[])
            ));
            $zip->finish();
        });

        return $response;
    }

    /**
     * @Route("/download/album/{id}", name="downloadAlbum", requirements={"id": "\d+"})
     * @param $id
     * @return StreamedResponse
     */
    public function downloadAlbumAction($id)
    {
        $album = new LeechzerAlbum(
            $this->getParameter('leechzer_entry_point'),
            User::getUserQuality($this->getUser()),
            new DeezerAlbum($id),
            User::getUserCover($this->getUser())
        );

        $response = new StreamedResponse(function () use ($album) {
            $zip = new ZipStream($album->getTitle().'.zip');
            $zip->setContent($album->getZipFiles());
            $zip->finish();
        });

        return $response;
    }
}
