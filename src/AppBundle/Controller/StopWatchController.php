<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StopWatchController extends Controller
{
    private $eventName;

    protected function startWatch($eventName)
    {
        $this->eventName = $eventName;
        $stopwatch = $this->get('debug.stopwatch');
        $stopwatch->start($this->eventName);
    }

    protected function stopWatch()
    {
        $stopwatch = $this->get('debug.stopwatch');
        $stopwatch->stop($this->eventName);
    }
}
