<?php

namespace AppBundle\Controller;

use AppBundle\Deezer\Artist;
use AppBundle\Deezer\Search;
use DeezerAPI\Models\Album;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class DeezerController extends StopWatchController
{
    /**
     * @Route("/search/{searchText}/{page}", requirements={"page": "\d+"})
     * @Route("/search/{searchText}")
     * @param $searchText
     * @param int $page
     * @return Response
     */
    public function searchAction($searchText, $page = 1)
    {
        $page = $page > 1 ? $page : 1;

        $this->startWatch('DeezerController:searchAction:deezerAPICall');
        $search = new Search($searchText, $page-1);
        $results = $search->search();
        $this->stopWatch();

        return $this->render('AppBundle:Deezer:search.html.twig', [
            'searchText' => $searchText,
            'results' => $results,
            'page' => $page,
        ]);
    }

    /**
     * @Route("/album/{id}")
     * @param $id
     * @return Response
     */
    public function albumAction($id)
    {
        $this->startWatch('DeezerController:albumAction:deezerAPICall');
        $album = new Album($id);

        foreach ($album->tracks->data as $track) {
            $track->album = $album;
        }
        $this->stopWatch();

        return $this->render('AppBundle:Deezer:album.html.twig', [
            'album' => $album,
        ]);
    }

    /**
     * @Route("/artist/{id}")
     * @param $id
     * @return Response
     */
    public function artistAction($id)
    {
        $this->startWatch('DeezerController:artistAction:deezerAPICall');
        $artist = new Artist($id);
        $this->stopWatch();

        return $this->render('AppBundle:Deezer:artist.html.twig', [
            'artist' => $artist,
        ]);
    }

    /**
     * @Route("/searchForm")
     * @param Request $request
     * @return Response
     */
    public function searchFormAction(Request $request)
    {
        $response = 'false';
        $searchForm = $this->container->get('form.factory')
            ->createBuilder(FormType::class, null, ['csrf_protection' => false])
            ->add('search', TextType::class, [
                'label' => false,
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new NotBlank(),
                    new Regex([
                        'pattern' => '/\//',
                        'match' => false
                    ]),
                ]
            ])
            ->getForm();
        $searchForm->handleRequest($request);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $response = $this->generateUrl('app_deezer_search', [
                'searchText' => $searchForm->get('search')->getData(),
            ]);
        }
        return new Response($response);
    }
}
