<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function indexAction(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('search', TextType::class, [
                'label' => 'Webchzer',
                'mapped' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'need-music',
                    'autofocus' => true
                ],
                'constraints' => [
                    new NotBlank(),
                    new Regex([
                        'pattern' => '/\//',
                        'match' => false
                    ]),
                ]
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            return $this->redirectToRoute('app_deezer_search', [
                'searchText' => $form->get('search')->getData(),
            ]);
        }

        return $this->render('@App/Default/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
