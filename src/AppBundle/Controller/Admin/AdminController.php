<?php

namespace AppBundle\Controller\Admin;

use AppBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/admin")
 */
class AdminController extends Controller
{
    /**
     * @Route("/", name="admin_index")
     */
    public function indexAction()
    {
        return $this->render('AppBundle:Admin:index.html.twig', [
            'usersCount' => $this->getUserRepository()->count(),
        ]);
    }

    /**
     * @return UserRepository
     */
    private function getUserRepository()
    {
        return $this->getDoctrine()->getRepository('AppBundle:User');
    }
}
