<?php

namespace AppBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Nietonfir\Google\ReCaptchaBundle\Controller\ReCaptchaValidationInterface;

class RegistrationController extends BaseController implements ReCaptchaValidationInterface
{
}
