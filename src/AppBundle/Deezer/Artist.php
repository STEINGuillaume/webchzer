<?php

namespace AppBundle\Deezer;

use DeezerAPI\Models\Artist as BaseArtist;

class Artist extends BaseArtist
{
    /**
     * @param int $limit
     * @return array
     */
    public function getTop($limit = 10)
    {
        $this->connectionData = array();

        $url = Search::DEEZER_API_URL . '/artist/' . $this->id . '/top?limit=' .$limit;

        $results = json_decode(file_get_contents($url));
        foreach ($results->data as $result) {
            array_push($this->connectionData, $result);
        }

        return $this->connectionData;
    }
}
