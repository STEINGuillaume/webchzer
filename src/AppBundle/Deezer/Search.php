<?php

namespace AppBundle\Deezer;

use DeezerAPI\Search as DeezerSearch;

class Search extends DeezerSearch
{
    private $page;

    public function __construct($query, $page)
    {
        $this->page = $page;
        parent::__construct($query, null, null, $limit = 15, false);
    }

    /**
     * @return array
     */
    public function search()
    {
        $queryString = array(
            'q'     => $this->query,
            'limit' => $this->limit,
            'index' => $this->page * $this->limit,
        );

        if ($this->order) {
            $queryString['order'] = $this->order;
        }

        $url = self::DEEZER_API_URL . '/search/' . $this->type . '?' . http_build_query($queryString);

        $this->parse($url);

        return $this->getAllData();
    }
}
