<?php

namespace AppBundle\Leechzer;

use DeezerAPI\Models\Album as DeezerAlbum;

class Album
{
    /**
     * @var Track[]
     */
    private $tracks = [];

    /**
     * @var string
     */
    private $title;

    /**
     * @var bool
     */
    private $add_cover;

    /**
     * @var DeezerAlbum
     */
    private $deezerAlbum;

    /**
     * Album constructor.
     * @param string $leechzer_entry_point
     * @param string $quality
     * @param DeezerAlbum $deezerAlbum
     * @param bool $add_cover
     */
    public function __construct($leechzer_entry_point, $quality, $deezerAlbum, $add_cover)
    {
        $this->title = $deezerAlbum->title;
        if (isset($deezerAlbum->tracks)) {
            foreach ($deezerAlbum->tracks->data as $track) {
                $this->tracks[] = new Track(
                    $leechzer_entry_point,
                    $quality,
                    $track->id,
                    $track->title,
                    $deezerAlbum->title,
                    $deezerAlbum->artist->name
                );
            }
        }
        $this->add_cover = $add_cover;
        $this->deezerAlbum = $deezerAlbum;
    }

    /**
     * @return ZipFile[]
     */
    public function getZipFiles()
    {
        return array_merge(
            $this->tracks,
            ($this->add_cover?[$this->getCover()]:[])
        );
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return Cover
     */
    private function getCover()
    {
        return (new Cover($this->deezerAlbum, $this->deezerAlbum->artist));
    }
}
