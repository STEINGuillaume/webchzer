<?php

namespace AppBundle\Leechzer;

use DeezerAPI\Models\Album as DeezerAlbum;
use DeezerAPI\Models\Artist as DeezerArtist;

class Cover extends ZipFile
{
    /**
     * Cover constructor.
     * @param DeezerAlbum|object $album
     * @param DeezerArtist|object $artist
     */
    public function __construct($album, $artist)
    {
        $this->name = 'Folder.jpg';
        $this->url = $album->cover_xl;
        $this->album = $album->title;
        $this->artist = $artist->name;
    }
}
