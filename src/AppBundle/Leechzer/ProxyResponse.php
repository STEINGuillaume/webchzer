<?php

namespace AppBundle\Leechzer;

use Symfony\Component\HttpFoundation\HeaderBag;
use Symfony\Component\HttpFoundation\Response;
use GuzzleHttp\Client;

class ProxyResponse extends Response
{
    public static $ALLOWED_REQUEST_HEADERS = [
        'range',
    ];

    public static $ALLOWED_RESPONSE_HEADERS = [
        'content-range',
        'content-type',
        'content-length',
        'accept-ranges',
        'x-leechzer-format',
        'x-leechzer-formats',
    ];

    private $fp;

    /**
     * ProxyResponse constructor.
     * @param string $requestUrl
     * @param HeaderBag $requestHeaders
     */
    public function __construct($requestUrl, $requestHeaders)
    {
        parent::__construct();
        $client = new Client;
        $headers = [];

        // Pass request headers to Leechzer
        foreach ($requestHeaders as $name => $values) {
            if (!in_array($name, self::$ALLOWED_REQUEST_HEADERS)) {
                continue;
            }

            $headers[$name] = $values;
        }

        // Send request and pass response headers
        $resp = $client->get($requestUrl, [
            'stream' => true,
            'headers' => $headers
        ]);
        $this->setStatusCode($resp->getStatusCode(), $resp->getReasonPhrase());

        foreach ($resp->getHeaders() as $name => $value) {
            if (!in_array(strtolower($name), self::$ALLOWED_RESPONSE_HEADERS)) {
                continue;
            }

            $this->headers->set($name, $value);
        }

        $this->fp = $resp->getBody()->detach();
    }

    public function sendContent()
    {
        fpassthru($this->fp);
        fclose($this->fp);
    }
}
