<?php

namespace AppBundle\Leechzer;

use GuzzleHttp\Client;

abstract class ZipFile
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $url;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var int
     */
    protected $size;

    /**
     * @var string
     */
    protected $artist;

    /**
     * @var string
     */
    protected $album;

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        if (!isset($this->path)) {
            $forbidden=['/','<','>',':','"','\\','|','?','*'];
            $replacedBy='-';
            $this->path = implode('/', [
                str_replace($forbidden, $replacedBy, $this->artist),
                str_replace($forbidden, $replacedBy, $this->album),
                str_replace($forbidden, $replacedBy, $this->getName()),
            ]);
        }
        return $this->path;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        if (!isset($this->size)) {
            $client = new Client;
            $resp = $client->head($this->getUrl());
            $this->size = (int)$resp->getHeader('content-length');
        }
        return $this->size;
    }

    public function getName()
    {
        return $this->name;
    }
}
