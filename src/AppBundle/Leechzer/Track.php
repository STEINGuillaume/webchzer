<?php

namespace AppBundle\Leechzer;

use GuzzleHttp\Client;

class Track extends ZipFile
{
    /**
     * @var string
     */
    private $ext;

    /**
     * Track constructor.
     * @param string $leechzer_entry_point
     * @param string $quality
     * @param int $id
     * @param string $title
     * @param string $album
     * @param string $artist
     */
    public function __construct($leechzer_entry_point, $quality, $id, $title, $album, $artist)
    {
        $this->artist = $artist;
        $this->album = $album;

        $this->url = implode('/', [
            $leechzer_entry_point,
            $quality,
            $id,
        ]);

        // Get actual track format and size using a HEAD request
        $client = new Client;
        $resp = $client->head($this->url);
        $format = $resp->getHeader('X-Leechzer-Format');
        $this->ext = '.' . strtolower(explode('_', $format)[0]);

        $this->size = (int)$resp->getHeader('content-length');
        $this->name = $title . $this->ext;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->ext;
    }
}
