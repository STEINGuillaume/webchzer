<?php

namespace AppBundle\EventListener;

use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\FOSUserEvents;
use Swift_Mailer;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class UserRegistrationListener implements EventSubscriberInterface
{
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var
     */
    private $from_email;

    public function __construct(Swift_Mailer $mailer, EngineInterface $templating, $from_email)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->from_email = $from_email;
    }

    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        ];
    }

    public function onRegistrationCompleted(FilterUserResponseEvent $event)
    {
        $rendered = $this->templating->render('@App/Email/UserRegistration/notifyAdmin.txt.twig', array(
            'user' => $event->getUser(),
        ));
        $renderedLines = explode("\n", trim($rendered));
        $subject = array_shift($renderedLines);
        $body = implode("\n", $renderedLines);

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom($this->from_email)
            ->setTo($this->from_email)
            ->setBody($body);

        $this->mailer->send($message);
    }
}
