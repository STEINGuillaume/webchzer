<?php

namespace AppBundle;

use AppBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class AppBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new OverrideServiceCompilerPass());
    }

    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
