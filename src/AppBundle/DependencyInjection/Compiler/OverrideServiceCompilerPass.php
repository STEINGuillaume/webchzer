<?php

namespace AppBundle\DependencyInjection\Compiler;

use AppBundle\Controller\RegistrationController;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('fos_user.registration.controller');
        $definition->setClass(RegistrationController::class);
    }
}
