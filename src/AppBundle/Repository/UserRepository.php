<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    public function count()
    {
        $qb = $this->createQueryBuilder('u');
        return $qb
            ->select('count(u.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findAllQuery()
    {
        return $this->_em->createQuery("SELECT u FROM AppBundle:User u");
    }
}
